﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Piano
{
	public partial class Piano : Form
	{
		Keyboard Board;

		public Piano()
		{
			InitializeComponent();
			Board = new Keyboard();
			this.MouseWheel += new MouseEventHandler(MouseWhl);
			

		}
		private void MouseWhl(object sender, MouseEventArgs e)
		{
			Board.ScroolNotes(e.Delta);
			pictureBox.Invalidate();
		}

		private void pictureBoxMouseClick(object sender, MouseEventArgs e)
		{
			Board.KeyBoardPressed(e.X, e.Y);
			Board.NoteSelected(e.X, e.Y);
			pictureBox.Invalidate();
		}
		private void pictureBoxPaint(object sender, PaintEventArgs e)
		{
			var g = e.Graphics;

			Board.BuildKeyBoard(g);
			Board.DrawSound(g);

		}
		private void Form1_KeyPress(object sender, KeyPressEventArgs e)
		{

			Board.DrawSound(e.KeyChar);

			if (e.KeyChar == ' ')
			{
				Board.position = 0;
				Board.playAll = true;
				foreach (var note in Board.Notes)
				{
					if (Board.position > note.Top) Board.position = note.Top;
				}
			}

			pictureBox.Invalidate();
		}
		private void timer1_Tick(object sender, EventArgs e)
		{
			//timer1.Enabled = false;
			Board.position++;
			if (Board.playAll)
			{
				Board.PlaySound();

			}

		}
		private void ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Board.WriteXML();
		}
		private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Board.ReadXML();
			pictureBox.Invalidate();
		}

		private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
		{

		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Board.Load();
		}

	}

	public class Note : NoteForSave
	{
		public FMOD.Sound sound = null;
		public FMOD.Channel channel = null;
	}
	public class NoteForSave
	{
		public bool white;
		public bool selected;
		public bool plays;
		public int Height { get; set; }
		public int Top { get; set; }
		public int whichNote { get; set; }
		public int octave { get; set; }
	}
	public class Channel
	{
		public FMOD.Channel channel;
		public int whichNote;
		public FMOD.Sound sound = null;
		public FMOD.RESULT result;	
	}

	class Keyboard
	{
		const int whiteKeyW = 45;
		const int whiteKeyH = 300;
		const int blackKeyW = 26;
		const int blackKeyH = 200;
		const int firstKeyX0 = 40;

		const int firstKeyY = 350;
		const int interval = 1;
		public int position = 0;
		public bool playAll = false;
		const int NoteHeight = 50;
		int firstKeyX = 100;
		public FMOD.System system = null, system1 = null, system2 = null;
		public bool first = true;

		public struct BlackKey
		{
			public Rectangle blackRct { get; set; }
			public int octave { get; set; }
			public int whichNote { get; set; }
		}

		public List<Channel> Channels = new List<Channel>();
		public List<Note> Notes = new List<Note>();
		public List<BlackKey> BlackKeys = new List<BlackKey>();
		public List<NoteForSave> NotesForSave = new List<NoteForSave>();

		double[] black = new double[] { 0.7, 1.8, 3.7, 4.8, 5.9 };
		public void BuildKeyBoard(Graphics graph)
		{
			for (int j = 0; j < 4; j++)
			{
				firstKeyX = (int)(firstKeyX0 + j * (whiteKeyW + interval) * 7);
				Rectangle R;
				for (int i = 0; i < 7; i++)
				{
					R = new Rectangle(firstKeyX + i * (whiteKeyW + interval), firstKeyY, whiteKeyW, whiteKeyH);
					graph.FillRectangle(Brushes.LightCoral, R);
				}
				for (int i = 0; i < 5; i++)
				{
					R = new Rectangle((int)(firstKeyX + black[i] * whiteKeyW), firstKeyY, blackKeyW, blackKeyH);
					graph.FillRectangle(Brushes.Black, R);
					BlackKeys.Add(new BlackKey()
					{
						blackRct = R,
						octave = j,
						whichNote = i
					});
				}
			}

			if (BlackKeys.Count > 20)
				BlackKeys.RemoveRange(20, 20);

		}

		public void KeyBoardPressed(int x, int y)
		{
			foreach (var bKey in BlackKeys)
				if ((x <= bKey.blackRct.X + blackKeyW && x >= bKey.blackRct.X) && (y <= bKey.blackRct.Y + blackKeyH && y >= bKey.blackRct.Y))
				{
					Console.WriteLine(BlackKeys.IndexOf(bKey));
					Note LastTheSame = Notes.FindLast(delegate(Note nt)
					{
						if (!nt.white && nt.octave == bKey.octave)
							return (nt.whichNote == bKey.whichNote);
						else
							return false;
					});

					Notes.Add(new Note()
					{
						Height = NoteHeight,
						whichNote = bKey.whichNote,
						selected = false,
						plays = false,
						white = false,
						octave = bKey.octave
					});

					if (LastTheSame != null)
						Notes[Notes.Count() - 1].Top = LastTheSame.Top + LastTheSame.Height + interval;
					else Notes[Notes.Count() - 1].Top = 10;

					return;
				}


			for (int j = 0; j < 4; j++)
			{
				firstKeyX = (int)(firstKeyX0 + j * (whiteKeyW + interval) * 7);
				for (int i = 0; i < 7; i++)
					if ((x <= firstKeyX + whiteKeyW + i * (whiteKeyW + interval) && x >= firstKeyX + i * (whiteKeyW + interval)) && (y <= firstKeyY + whiteKeyH && y >= firstKeyY))
					{
						//SoundPlayer note = new SoundPlayer("C:\\notes\\" + (i + 1) + ".wav");
						//	note.Play();

						Note LastTheSame = Notes.FindLast(delegate(Note nt)
						{


							if (nt.white && nt.octave == j)
								return nt.whichNote == i;
							return false;

						});

						Notes.Add(new Note()
						{
							Height = NoteHeight,
							whichNote = i,
							selected = false,
							plays = false,
							white = true,
							octave = j
						});

						if (LastTheSame != null)
							Notes[Notes.Count() - 1].Top = LastTheSame.Top + LastTheSame.Height + interval;
						else Notes[Notes.Count() - 1].Top = 10;

						break;

					}
			}
		}
		public void PlaySound()
		{
			foreach (var note in Notes)
			{


				if (note.plays)
				{
					if (!((position >= note.Top) && (position <= note.Top + note.Height)))
					{
						note.plays = false;
						//system.playSound(FMOD.CHANNELINDEX.REUSE, note.sound, true, ref note.channel);
						//note.system.release();
						if (note.white)
						{
							foreach (var ch in Channels)
								if (ch.whichNote == note.octave * 100 + note.whichNote * 10)
								{
									system1.playSound(FMOD.CHANNELINDEX.REUSE, ch.sound, true, ref ch.channel);
									break;
								}
						}
						else
							foreach (var ch in Channels)
								if (ch.whichNote == note.octave * 100 + note.whichNote +1)
								{
									system2.playSound(FMOD.CHANNELINDEX.REUSE, ch.sound, true, ref ch.channel);
									break;
								}

					}
				}
				else
					if ((position >= note.Top) && (position <= note.Top + note.Height))
					{
						note.plays = true;
						if (note.white)
						{
							foreach (var ch in Channels)
								if (ch.whichNote == note.octave * 100 + note.whichNote * 10)
								{
									system1.playSound(FMOD.CHANNELINDEX.REUSE, ch.sound, false, ref ch.channel);
									break;
								}
						}
						else
							foreach (var ch in Channels)
								if (ch.whichNote == note.octave * 100 + note.whichNote + 1)
								{
									system2.playSound(FMOD.CHANNELINDEX.REUSE, ch.sound, false, ref ch.channel);
									break;
								}
					}



			}

			if (position > 1500)
			{
				//playAll = true;
				first = false;

			/*	foreach (var note in Notes)
				{

					if (position > note.Top) position = note.Top;
				}*/

			}

		}
		public void DrawSound(Graphics graph)
		{
			foreach (var note in Notes)
			{
				if (note.white)
				{
					Rectangle R = new Rectangle((int)(firstKeyX0 + note.octave * (whiteKeyW + interval) * 7) + note.whichNote * (whiteKeyW + interval), note.Top, whiteKeyW, note.Height);
					if (note.selected)
					{
						graph.FillRectangle(Brushes.Maroon, R);

					}
					else
						graph.FillRectangle(Brushes.LightGreen, R);
					//Console.WriteLine(note.whichNote);
				}
				else
				{
					if (note.selected)
					{
						graph.FillRectangle(Brushes.Maroon, BlackKeys[note.whichNote].blackRct.X + note.octave * (whiteKeyW + interval) * 7, note.Top, blackKeyW, note.Height);
					}
					else
						graph.FillRectangle(Brushes.MediumPurple, BlackKeys[note.whichNote].blackRct.X + note.octave * (whiteKeyW + interval) * 7, note.Top, blackKeyW, note.Height);
				}

			}
		}
		public void DrawSound(char key)
		{
			foreach (var note in Notes)
				if (note.selected)
				{
					if (key == 'й')
						note.Top += 6;
					else if (key == 'ц')
						note.Top -= 6;
					else if (key == '+')
						note.Height++;
					else if (key == '-')
						note.Height--;
					else if (key == 'я')
					{
						Notes.Remove(note);
						break;
					}
				}

		}

		public void NoteSelected(int x, int y)
		{

			foreach (var note in Notes)
			{
				if (note.white)
					if ((x <= (int)(firstKeyX0 + note.octave * (whiteKeyW + interval) * 7) + whiteKeyW + note.whichNote * (whiteKeyW + interval) && x >= (int)(firstKeyX0 + note.octave * (whiteKeyW + interval) * 7) + note.whichNote * (whiteKeyW + interval)) && (y <= note.Top + note.Height && y >= note.Top))
					{
						//Console.WriteLine(note.whichNote + " " + Notes.IndexOf(note));


						note.selected = true;
						//break;

					}
					else note.selected = false;
				else
				{
					if ((x <= BlackKeys[note.whichNote].blackRct.X + note.octave * (whiteKeyW + interval) * 7 + blackKeyW && x >= BlackKeys[note.whichNote].blackRct.X + note.octave * (whiteKeyW + interval) * 7) && (y <= note.Top + note.Height && y >= note.Top))
						note.selected = true;
					else note.selected = false;

				}


			}
		}
		public void WriteXML()
		{
			//	Book overview = new Book();
			//	overview.title = "pussy";
			NotesForSave.Clear();
			foreach (var note in Notes)
				NotesForSave.Add(new NoteForSave()
					{
						Height = note.Height,
						plays = false,
						selected = note.selected,
						Top = note.Top,
						whichNote = note.whichNote,
						white = note.white,
						octave = note.octave
					});


			System.Xml.Serialization.XmlSerializer writer =
				new System.Xml.Serialization.XmlSerializer(typeof(List<NoteForSave>));

			System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\Дима\Desktop\SerializationOverview.xml");
			writer.Serialize(file, NotesForSave);
			file.Close();

			NotesForSave.Clear();
		}
		public void ReadXML()
		{
			System.Xml.Serialization.XmlSerializer reader =
				new System.Xml.Serialization.XmlSerializer(typeof(List<NoteForSave>));
			System.IO.StreamReader file = new System.IO.StreamReader(
				@"C:\Users\Дима\Desktop\SerializationOverview.xml");

			NotesForSave = (List<NoteForSave>)reader.Deserialize(file);
			file.Close();
			//Console.WriteLine(overview.title);
			Notes.Clear();

			foreach (var noteFs in NotesForSave)
				Notes.Add(new Note()
				{
					Height = noteFs.Height,
					plays = false,
					selected = noteFs.selected,
					Top = noteFs.Top,
					whichNote = noteFs.whichNote,
					white = noteFs.white,
					octave = noteFs.octave
				});
			NotesForSave.Clear();

		}
		public void ScroolNotes(int Delta)
		{
			if (Delta >= 0)
				foreach (var note in Notes)
				{
					note.Top -= 20;

				}
			else
				foreach (var note in Notes)
				{
					note.Top += 20;

				}
		}

		public void Load()
		{
			FMOD.Factory.System_Create(ref system1);
			system1.init(32, FMOD.INITFLAGS.NORMAL, IntPtr.Zero);
			FMOD.Factory.System_Create(ref system2);
			system2.init(32, FMOD.INITFLAGS.NORMAL, IntPtr.Zero);

			for (int i = 0; i < 3; i++)
			{


				for (int j = 0; j < 7; j++)
				{
					Channels.Add(new Channel()
					{
						channel = null,
						whichNote = i * 100 + j * 10,
					});
					Console.WriteLine("{0}, {1}: {2}", i, j, i * 100 + j * 10);
					system1.createSound(@"C:\notes\" + ((int)(j) + (int)(1)) + "_" + i + ".wav", FMOD.MODE.SOFTWARE, ref (Channels[Channels.Count - 1].sound));
				}
				
				for (int j = 0; j < 5; j++)
				{
					Channels.Add(new Channel()
					{
						channel = null,
						whichNote = i * 100 + j +1,
					});

					Console.WriteLine("{0}, {1}: {2}", i, j, i * 100 + j + 1);
					system2.createSound(@"C:\notes\" + ((int)(j) + (int)(1)) + "#_" + i + ".wav", FMOD.MODE.SOFTWARE, ref Channels[Channels.Count - 1].sound);
				}

				Console.WriteLine();
			}
		}
	}
}